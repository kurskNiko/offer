<?php

use yii\db\Migration;

/**
 * Handles the creation of table `fonts`.
 */
class m190315_134326_create_fonts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('fonts', [
            'id' => $this->primaryKey(),
            'name' => $this->string(20),
            'url' => $this->string(150),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('fonts');
    }
}
