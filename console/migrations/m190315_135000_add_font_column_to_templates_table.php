<?php

use yii\db\Migration;

/**
 * Handles adding font to table `templates`.
 * Has foreign keys to the tables:
 *
 * - `fonts`
 */
class m190315_135000_add_font_column_to_templates_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('templates', 'font', $this->integer(10));

        // creates index for column `font`
        $this->createIndex(
            'idx-templates-font',
            'templates',
            'font'
        );

        // add foreign key for table `fonts`
        $this->addForeignKey(
            'fk-templates-font',
            'templates',
            'font',
            'fonts',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `fonts`
        $this->dropForeignKey(
            'fk-templates-font',
            'templates'
        );

        // drops index for column `font`
        $this->dropIndex(
            'idx-templates-font',
            'templates'
        );

        $this->dropColumn('templates', 'font');
    }
}
