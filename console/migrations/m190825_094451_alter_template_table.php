<?php

use yii\db\Migration;

/**
 * Class m190825_094451_alter_template_table
 */
class m190825_094451_alter_template_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('templates', 'header_color', 'varchar(255)');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190825_094451_alter_template_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190825_094451_alter_template_table cannot be reverted.\n";

        return false;
    }
    */
}
