<?php

namespace common\models\loader\parsers;

use common\models\loader\Parser;
use SimpleXMLElement;
use Yii;
use backend\models\brands\Brands;
use yii\helpers\ArrayHelper;
use backend\models\products\Products;


class Zavod extends Parser
{
    private $_xml;

    public function Parsing()
    {
        $data = file_get_contents($this->file);
        $data = str_replace('<Каталог>','<catalog>',$data);
        $data = str_replace('</Каталог>','</catalog>',$data);
        $data = str_replace('<Продукт>','<product>',$data);
        $data = str_replace('</Продукт>','</product>',$data);
        $data = str_replace('<Цена>','<price>',$data);
        $data = str_replace('</Цена>','</price>',$data);
        $data = str_replace('<Изображения>','<pictures>',$data);
        $data = str_replace('</Изображения>','</pictures>',$data);
        $data = str_replace('<Изображение>','<picture>',$data);
        $data = str_replace('</Изображение>','</picture>',$data);
        $data = str_replace('<Наименование>','<name>',$data);
        $data = str_replace('</Наименование>','</name>',$data);
        $data = str_replace('<Описание>','<description>',$data);
        $data = str_replace('</Описание>','</description>',$data);
        $data = str_replace('<Производитель>','<vendor>',$data);
        $data = str_replace('</Производитель>','</vendor>',$data);
        $data = str_replace('<Артикул>','<code>',$data);
        $data = str_replace('</Артикул>','</code>',$data);

  
        $this->_xml = new SimpleXMLElement($data);
        $space = $this->_xml->getNamespaces(true);
  
        $elems = [];
      
        foreach ($this->_xml->product as $g) {

            $g_image_link = $g->pictures->picture;
            $g_title = htmlspecialchars_decode($g->name);
            $g_seria = $g->vendor;
            $g_descr = $g->description;
            $g_descr = strip_tags($g_descr);
            $g_price = $g->price;
            $g_id = $g->code;

            $status = "Неизвестно";
            $price = intval(trim($g_price));

            if ($g_descr == "") {
                $g_descr = "Описание отсутствует";
            }

            $dir_path = Yii::getAlias('@xml');

            $url = $dir_path . '/' . $this->imagedir . '/';

            $thumbnail = '/uploads/xml/' . $this->imagedir . '/thumbnails/' . basename($g_image_link);

            try{
                $image_url = '/uploads/xml/' . $this->imagedir . '/' . basename($g_image_link);
                $this->getImage($url,$g_image_link, basename($g_image_link));
            }catch (yii\base\ErrorException $e){
                $image_url = '/uploads/images/noImage.jpg';
                $thumbnail = '/uploads/images/noImage.jpg';
            }

            array_push($elems, [
                "name" => $g_title,
                "description" => $g_descr,
                "articul" => $g_id,
                "status_goods" => $status,
                "postavshik" => $this->id,
                "tbl_brand" => $g_seria,
                "price" => $price,
                "photo" => $image_url,
                'thumb' => $thumbnail,
            ]);
        
        }

        $this->InsertBrand($elems);
        $this->InsertOrUpdate($elems);
    }
}
