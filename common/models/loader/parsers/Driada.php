<?php

namespace common\models\loader\parsers;

use common\models\loader\Parser;
use SimpleXMLElement;
use Yii;
use backend\models\brands\Brands;
use yii\helpers\ArrayHelper;
use backend\models\products\Products;
use yii\imagine\Image;
use Imagine\Image\Box;


class Driada extends Parser
{
    private $_xml;

    public function Parsing()
    {
        $data = file_get_contents($this->file);
  
        $this->_xml = new SimpleXMLElement($data);
        $space = $this->_xml->getNamespaces(true);

        $elems = [];

        foreach ($this->_xml->shop->offers->offer as $g) {
            $g_title = htmlspecialchars_decode($g->model);
            $g_seria = $g->vendor;
            $g_descr = "";
            $g_price = $g->price;
            $g_id = $g->vendorCode;

///////////////////
   
            if(!empty($g->url) && (strpos($g->url,'http://') == "false")){
                $c = curl_init($g->url);
                curl_setopt($c, CURLOPT_RETURNTRANSFER, true);

                $content = curl_exec($c);
                $description = curl_exec($c);
                if (curl_error($c))
                    die(curl_error($c));

                $status = curl_getinfo($c, CURLINFO_HTTP_CODE);
                curl_close($c);
            
                $position = strpos($content, 'src="data/medium');
                $content = substr($content, $position);
                $content = substr($content, 0,100);
                $content = explode('"',$content);

                //Вытаскиваем описание
                if ($g_descr == ""){
                    $dposition = strpos($description, '<td width=99% valign=top>');
                    $description = substr($description, $dposition);
                    $dposition = strpos($description, '<p>');
                    $description = substr($description, $dposition);
                    $description = explode('>',$description);
                    $description = str_replace("</p","",$description[1]);
                    $description = strip_tags(iconv( "cp1251","UTF-8", $description));
                    if(strlen($description)< 25){ 
                        $g_descr = "Описание отсутствует";
                    }
                    ///
                } else  {
                    $g_descr = $g->description;
                }
                
                $dir_path = Yii::getAlias('@xml');
                $url = $dir_path . '/' . $this->imagedir . '/';
                
                $g_image_start = "http://driada-sport.ru/".$content[1];

                $image_url = '/uploads/xml/' . $this->imagedir . '/' . basename($g_image_start);
                $thumbnail = '/uploads/xml/' . $this->imagedir . '/thumbnails/' . basename($g_image_start);

                try{
                    $image_url = '/uploads/xml/' . $this->imagedir . '/' . basename($g_image_start);
                    $this->getImage($url,$g_image_start, basename($g_image_start));
                }catch (yii\base\ErrorException $e){
                    $image_url = '/uploads/images/noImage.jpg';
                    $thumbnail = '/uploads/images/noImage.jpg';
                }
            }else{
                continue;
            }


            $status = "Неизвестно";
            $price = intval(trim($g_price));

            array_push($elems, [
                "name" => $g_title,
                "description" => $g_descr,
                "articul" => $g_id,
                "status_goods" => $status,
                "postavshik" => $this->id,
                "tbl_brand" => $g_seria,
                "price" => $price,
                "photo" => $image_url,
                'thumb' => $thumbnail,
            ]);
            
        }

        $this->InsertBrand($elems);
        $this->InsertOrUpdate($elems);

    }
}
