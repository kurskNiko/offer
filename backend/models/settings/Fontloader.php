<?php

namespace backend\models\settings;

use yii\helpers\FileHelper;
use Yii;
use yii\web\UploadedFile;
use yii\base\Model;
use backend\models\settings\Fonts;

class Fontloader extends Model
{
    public $name;
    public $font;

    public function rules()
    {
        return [
            ["font","required"]
        ];
    }

    public function attributeLabels()
    {
        return
            [
           
            ];
    }

    public function uploadFont()
    {
        $fontobj = new Fonts();

        $root = Yii::getAlias('@images');
        $path = $root . '/uploads/fonts/';
        if(!file_exists($path)){
          FileHelper::createDirectory($path, 0775, true);
        }
       

        /**
         * Сохраняем
         */
        $this->font->saveAs($path . $this->font->baseName . '.' . $this->font->extension);
      //  $this->font->saveAs(Yii::getAlias('@vendor') . "/mpdf/mpdf/ttfonts" . $this->font->baseName . '.' . $this->font->extension);

       
        $fontobj->name = $this->font->baseName;
        $fontobj->url = $path . $this->font->baseName . '.' . $this->font->extension;

           //Добавляем в ксс шрифт
             $txt = "
             @font-face {
               font-family: '".$this->font->baseName."';
               src: url('../../uploads/fonts/".$this->font->baseName . '.' . $this->font->extension."');
             }
           ";
             
           $fontcss = fopen('../../frontend/app/fonts.css','a');
           fwrite($fontcss, "\n". $txt);
           fclose($fontcss);
           //
      
        if ($fontobj->save()) {
            $response = 'Успешно загружен';
        }

        return $response;
    }

}