<?php

namespace backend\models\settings;

use yii\db\ActiveRecord;
use backend\models\templates\Templates;

class Fonts extends ActiveRecord
{   
    public function getTemplates() {
        return $this->hasMany(Templates::className(), ['font' => 'id']);
    }
}

?>