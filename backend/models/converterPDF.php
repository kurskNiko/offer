<?php
/**
 * Created by PhpStorm.
 * User: huge
 * Date: 04.12.18
 * Time: 17:59
 */

namespace backend\models;

use Yii;
use Mpdf\Mpdf;
use Mpdf\Config\FontVariables;

class converterPDF
{
    public function convert($post){
          $fontconf = new FontVariables();
          $fontdatabody = $fontconf->getDefaults();
       
          $fontData = $fontdatabody['fontdata'];
       
           $pdf = new Mpdf([
           // set to use core fonts only
           'mode' => 'utf-8',
           // A4 paper format
           'format' => 'A4',
           // portrait orientation
         //  'orientation' => 'L',
           // stream to browser inline
           'orientation' => 0,
            'margin_left' => 0,
            'margin_right' => 0,
            'margin_top' => 0,
            'margin_bottom' => 0,
            'margin_header' => 0,
            'margin_footer' => 0,
           'destination' => 'F',
         
           // your html content input
           'content' => $post['body'],
           // format content from your own css file if needed or use the
           // enhanced bootstrap css built by Krajee for mPDF formatting
           'cssFile' => '../../frontend/app/dist/css/app.css',
           // any css to be embedded if required
           'cssInline' => '',
           'default_font_size' => 11,
            'fontdata' => $fontData+[
              strtolower($post['font']) => [
                "R" => "../../../../uploads/fonts/".$post['font'].".ttf",
                "B" => "../../../../uploads/fonts/".$post['font'].".ttf"
              ],
            ],
           'methods' => [
               'SetHeader'=>[''],
               'SetFooter'=>[''],
           ],
           'table_error_report' => true
       ]);
    
        $filename = md5($post['body']);
        $stylesheet = file_get_contents('../../frontend/app/dist/css/app.css');
       // $pdf->debug = true;
      //  $pdf->ignore_table_widths = true;
      //  $pdf->tableMinSizePriority = true;
      $pdf->SetDisplayMode('fullpage');
 
        $pdf->WriteHTML($stylesheet,1);
        $pdf->WriteHTML($post['body'],2);
      //  $mpdf->list_indent_first_level = 0;
        $pdf->SetHeader('');
    //    $pdf->SetHTMLFooter('<div class="row"><div class="col-12 p-0"><div class="template-data footer-template">'.$post['footer'].'</div></div></div>');
        $pdf->Output('../../uploads/pdf/'.$filename.'.pdf', 'F'); 

        return '/uploads/pdf/'.$filename.'.pdf';
    }


}