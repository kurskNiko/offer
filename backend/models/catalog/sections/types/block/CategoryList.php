<?php

namespace backend\models\catalog\sections\types\block;

use backend\models\catalog\sections\types\block\Block;
use yii\base\BaseObject;
use backend\models\catalog\sections\Sections;

class CategoryList extends BaseObject implements Block
{
    public $tree = [];

    public $result = [];

    public function formData($request)
    {
        return self::renderHierarchy();
    }

    private function renderHierarchy()
    {   
        $roots = Sections::find()->roots()->all();
        foreach ($roots as $leaf) {
            array_push($this->result,[ 
            'id' => $leaf->id,
            'name' => $leaf->name,
            'childs' => self::getChilds($leaf),    
            'vision' => false,   
            ]);
        }

        return $this->result;
    }

    private function getChilds($parent){
        $result = [];
        $items = Sections::findOne(['name' => $parent->name]);
        $items = $items->children(1)->all();
        
        foreach ($items as $leaf) {
            array_push($result,[ 
            'id' => $leaf->id,
            'name' => $leaf->name,
            'childs' => self::getChilds($leaf),
            'vision' => false,    
            ]);
        }
        return $result;
    }
}