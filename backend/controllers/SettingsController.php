<?
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;
use backend\models\settings\Fontloader;
use yii\web\UploadedFile;
use backend\models\settings\Fonts;
use backend\models\templates\Templates;

/**
 * Users controller
 */
class SettingsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [''],
                        'allow' => true,
                        'roles' => ['manager-role','product-role'],
                    ],
                    [
                        'actions' => ['index','fonts','loadfont','delfont'],
                        'allow' => true,
                        'roles' => ['admin-role'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {  
        return $this->render('index.twig');
    }

    public function actionFonts(){
        $fonts = Fonts::find()->asArray()->all();
        
        return $this->render('fonts.twig',[
            'fonts' => $fonts,
            'token' => Yii::$app->request->getCsrfToken(),
        ]);
    }

    public function actionDelfont(){
        $id = Yii::$app->request->get('id_del');
        $font = Fonts::findOne($id);
        $template = Templates::find()->where(['=','font',$id])->all();

        foreach($template as $t){
            $t->unlink('font',$font);
        }
        
        $font->delete();

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionLoadfont(){
        $validator = new Fontloader();
        $error = "Шрифт загружен успешно";

        if (!empty(Yii::$app->request->post())) {
            $response = Yii::$app->response;
            $response->format = \yii\web\Response::FORMAT_JSON;

            $validator->load(Yii::$app->request->post());
            $validator->name = Yii::$app->request->post('name');
            $validator->font = UploadedFile::getInstanceByName('font');

            if ($validator->validate()) {  
                $response->data = $validator->uploadFont();
            }

            return $this->redirect(['settings/fonts', [
                'token' => Yii::$app->request->getCsrfToken(),
                'error' => $error,
            ]], 301);
        } else{
            $error = 'Ошибка при загрузке';
            return $this->redirect(['settings/fonts', [
                'token' => Yii::$app->request->getCsrfToken(),
                'error' => $error,
            ]], 301);
        }   
    }
}    