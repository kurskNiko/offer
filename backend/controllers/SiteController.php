<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use yii\helpers\Url;
use common\models\loader\parsers\Loader;
use yii\web\UploadedFile;
use backend\models\merchants\Merchant;
use backend\models\products\Products;
use backend\models\templates\Templates;
use backend\models\Users;
use backend\models\catalog\sections\types\block\TreeCreator;
use backend\models\catalog\sections\types\block\CategoryCreator;
use backend\models\brands\Brands;
use backend\models\currency\Currency;
use backend\models\catalog\sections\Sections;
use yii\helpers\ArrayHelper;
use backend\models\converterPDF;
use backend\models\offers\Offers;
use yii2tech\spreadsheet\Spreadsheet;
use yii\data\ArrayDataProvider;
use yii\helpers\Json;
use backend\models\settings\Fonts;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error', 'getprods','alldata','getpdf','customlogin','getcsrf','saveorder','filterprods','categorylist','getexel'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'getprods','alldata','getpdf','customlogin','getcsrf','saveorder','filterprods','categorylist','getexel'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['parsers', 'getprods','alldata','getpdf','customlogin','getcsrf','saveorder','filterprods','categorylist','getexel'],
                        'allow' => true,
                        'roles' => ['admin-role', 'product-role'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    // restrict access to
                    'Access-Control-Allow-Origin' => ['*'],
                    'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                    // Allow only POST and PUT methods
                    'Access-Control-Request-Headers' => ['*'],
                    // Allow only headers 'X-Wsse'
                    'Access-Control-Allow-Credentials' => true,
                    // Allow OPTIONS caching
                    'Access-Control-Max-Age' => 86400,
                    // Allow the X-Pagination-Current-Page header to be exposed to the browser.
                    'Access-Control-Expose-Headers' => [],
                ]
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionSaveorder(){
        $this->enableCsrfValidation = false;

        $post = Yii::$app->request->post();
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;

        $offer = new Offers();
        $offer->user_id = $post['user'];
        $offer->template_id = $post['template'];
        $offer->data = json_encode($post['data']);
        $offer->main_sale = $post['sale'];
        $offer->name = $post['name'];

        if($offer->save()){
            $response->data = 'Шаблон удачно сохранен';
        }
        return $response;
    }

    public function actionGetprods()
    {
        $products = new Products;
        $data = $products->find()->asArray()->all();
        

        foreach ($data as &$item){
           $product = Products::findOne($item['id']);
           $sections = $product->sections;
           $brands = $product->brands;
           $merchs = $product->merchant;

           $sections = ArrayHelper::toArray($sections, [
                Sections::className() => [
                    'id',
                    'name',
                ],
            ]);

            $brands = ArrayHelper::toArray($brands, [
                Brands::className() => [
                    'id',
                    'name',
                ],
            ]);

            $merchs = ArrayHelper::toArray($merchs, [
                Merchant::className() => [
                    'id',
                    'name',
                ],
            ]);

            ArrayHelper::setValue($item, ['sections'], $sections);
            ArrayHelper::setValue($item, ['brands'], $brands);
            ArrayHelper::setValue($item, ['merchants'], $merchs);
        }


        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        $response->data = $data;

        return $response;
    }

    public function actionGetpdf(){
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        $response->data = '';

        $post = Yii::$app->request->post();

        $converter = new converterPDF();

        $response->data = $converter->convert($post);

        return $response;
    }
    
    public function actionFilterprods(){
        $sect = Sections::findOne(Yii::$app->request->post('cat_id'));

        $childs = $sect->children()->all();

        $data = $sect->getProducts()->asArray()->all();

        //Добавляем категории, бренды, секции
        foreach($data as &$val){  
            $product = Products::findOne($val['id']);
            $sections = $product->sections;
            $brands = $product->brands;
            $merchs = $product->merchant;

            $sections = ArrayHelper::toArray($sections, [
                Sections::className() => [
                    'id',
                    'name',
                ],
            ]);

            $brands = ArrayHelper::toArray($brands, [
                Brands::className() => [
                    'id',
                    'name',
                ],
            ]);

            $merchs = ArrayHelper::toArray($merchs, [
                Merchant::className() => [
                    'id',
                    'name',
                ],
            ]);

            ArrayHelper::setValue($val, ['sections'], $sections);
            ArrayHelper::setValue($val, ['brands'], $brands);
            ArrayHelper::setValue($val, ['merchants'], $merchs);    
        }
        //

         foreach($childs as $child){
            $sect_ref = Sections::findOne($child['id']);
            $data_ref = $sect_ref->getProducts()->asArray()->all();

            //Добавляем категории, бренды, секции
            foreach($data_ref as &$val){  
                $product = Products::findOne($val['id']);
                $sections = $product->sections;
                $brands = $product->brands;
                $merchs = $product->merchant;

                $sections = ArrayHelper::toArray($sections, [
                    Sections::className() => [
                        'id',
                        'name',
                    ],
                ]);

                $brands = ArrayHelper::toArray($brands, [
                    Brands::className() => [
                        'id',
                        'name',
                    ],
                ]);

                $merchs = ArrayHelper::toArray($merchs, [
                    Merchant::className() => [
                        'id',
                        'name',
                    ],
                ]);

                ArrayHelper::setValue($val, ['sections'], $sections);
                ArrayHelper::setValue($val, ['brands'], $brands);
                ArrayHelper::setValue($val, ['merchants'], $merchs);    
            }
            //

            $data = array_merge($data,$data_ref);
        }

        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        $response->data = $data;

        return $response;
    }

    public function actionAlldata(){
       $currentUser = Yii::$app->request->post('user');

       $data['templates'] = Templates::find()->asArray()->all();

       foreach($data['templates'] as &$template){
           $fontname = Fonts::find()->asArray()->where(['=','id',$template['font']])->one();
           array_push($template,['fontname' => $fontname['name'],'fonturl' => $fontname['url']]);
       }

       $data['users'] = Users::UsersForApi();
       
       $data['catalog'] = Sections::find()->asArray()->orderBy('tree,lft')->all();
       $data['brands'] = Brands::find()->asArray()->all();
       $data['merchants'] = Merchant::find()->asArray()->all();
       $data['currency'] = Currency::find()->asArray()->all();
       $data['user_carts'] = Offers::find()->asArray()->where(['=','user_id',$currentUser])->all();

        $brands = Products::find()->asArray()->select(['brand_id','merchant_id'])->distinct()->all();

        $brands_arr = [];
        foreach($brands as $brand){
             $brand_info = Brands::find()->asArray()->select(['id','name'])->where(['=','id',$brand['brand_id']])->all();   
             $merch_info = Merchant::find()->select('id')->where(['=','id',$brand['merchant_id']])->all(); 
             array_push($brands_arr, ['merch_id' => $merch_info[0]->id, 'brand_info'=> $brand_info]);
        }

        $data['brand_links'] = $brands_arr;

        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        $response->data = $data;

        return $response;
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            Yii::$app->response->redirect(Url::to('/backend/web/'));
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionParsers()
    {
        $this->enableCsrfValidation = false;
        if (Yii::$app->request->post()) {

            $response = 'Товары успешно загружены';

            $post = Yii::$app->request->post();
     
            $file = UploadedFile::getInstanceByName('xml');

            //Проверяем указан ли новый загрузчик
               if(!isset($post['loadernew'])){
                  $post['loadernew'] = 0;
                }

            $loader = new Loader();

            try {
                $parser = $loader->createParser($post, $file);
                $parser->Parsing();
            } catch (yii\base\ErrorException $e) {
                $response = 'Для данного поставщика отсутствует загрузчик. Обратитесь к администратору.';
            }

            return $this->render('parsed.twig', ['resp' => $response]);

        } else {
            $merch = new Merchant();
            $merch_arr = $merch->find()->Select(['name', 'id'])->asArray()->all();

            return $this->render('parsers.twig', [
                'link' => Url::toRoute('site/parsers'),
                'merches' => $merch_arr,
            ]);
        }
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionCustomlogin(){
        $post = Yii::$app->request->post();
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        $response->data = Users::CustomLoginIn(Yii::$app->request->post());
        return $response;
    }

    public function actionGetcsrf(){
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        $response->data = Yii::$app->request->getCsrfToken();
        return $response;
    }

    public function actionGetexel(){
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        $order = Yii::$app->request->post();
        $order = JSON::decode($order['body']);
        $exeldata = $order['data'];

        foreach($exeldata as &$ex){
            $ex['merchants'] = $ex['merchants']['name'];
            $ex['brands'] = $ex['brands']['name'];
            $ex['indsale'] = $ex['indsale'].'%';
        }

        $exporter = new Spreadsheet([
            'dataProvider' => new ArrayDataProvider([
                'allModels' => $exeldata
            ]),
            'columns' => [
                [
                    'attribute' => 'id',
                    'contentOptions' => [
                        'alignment' => [
                            'horizontal' => 'center',
                            'vertical' => 'center',
                        ],
                    ],
                ],
                [
                    'attribute' => 'name',
                ],
                [
                    'attribute' => 'articul',
                ],
                [
                    'attribute' => 'merchants',
                ],
                [
                    'attribute' => 'brands',
                ],
                [
                    'attribute' => 'price_roznica',
                ],
                [
                    'attribute' => 'quant',
                ],
                [
                    'attribute' => 'indsale',
                ],
            ],
        ]);

        $length = count($order['data'])+2;
        $title_length = $length+1;
        $numeric_length = $length+2;

        $height_summ = 'F'.$numeric_length;
        $height_sale = 'G'.$numeric_length;
        $height_sale_summ = 'H'.$numeric_length;

        $height_summ_title = 'F'.$title_length;
        $height_sale_title = 'G'.$title_length;
        $height_summ_sale_title = 'H'.$title_length;

        $grid = $exporter->render();
        $grid->renderCell($height_summ_title,'Общая сумма');
        $grid->renderCell($height_sale_title,'Общая скидка');
        $grid->renderCell($height_summ_sale_title,'Общая сумма со скидкой');

        $grid->renderCell($height_summ,$order['summ'][2]);
        $grid->renderCell($height_sale,$order['summ'][0]);
        $grid->renderCell($height_sale_summ,$order['summ'][1]);

        $grid->renderCell('A1', 'ID');
        $grid->renderCell('B1', 'Наименование');
        $grid->renderCell('C1', 'Артикул');
        $grid->renderCell('D1', 'Поставщик');
        $grid->renderCell('E1', 'Бренд');
        $grid->renderCell('F1', 'Цена');
        $grid->renderCell('G1', 'Количество');
        $grid->renderCell('H1', 'Скидка');

        $filename = md5(rand(5,1213123321));    

        $grid->save('../../uploads/exel/'.$filename.'.xls');

        $response->data = '/uploads/exel/'.$filename.'.xls';

        return $response;
    }

    public function actionCategorylist(){
        $list = new CategoryCreator();
        $list_obj = $list->factory();

        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        $response->data = $list_obj->formData([]); 
        return $response;
    }
}
