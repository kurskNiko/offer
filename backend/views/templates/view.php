<?
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
use backend\models\templates\TemplatesValidator;

?>

<h2>Изменение шаблона</h2>
<?=$error?>

<?

$form = ActiveForm::begin([
    'id' => 'templates-update',
    'options' => ['class' => ''],
]) ?>
<? $model = new TemplatesValidator();
$model->getTemplateData();
?>
<?= $form->field($model, 'name')->textInput()->label('Название'); ?>
<?= $form->field($model, 'header_color')->textInput()->label('Цвет шапки таблицы'); ?>
<div style="<?="position:relative;top:-50px;left:410px;width:30px;height:30px;background-color:".$model->header_color?>"></div>
<?= $form->field($model, 'template')->label('Шаблон')->widget(CKEditor::className(),[
    'editorOptions' => ElFinder::ckeditorOptions('elfinder',[])
]);?>
<?= $form->field($model, 'template_footer')->label('Шаблон футер')->widget(CKEditor::className(),[
    'editorOptions' => ElFinder::ckeditorOptions('elfinder',[])
]);?>

<?= $form->field($model, 'template_footer')->textArea(['rows'=>10])->label('Шаблон футер Html') ?>

<?= $form->field($model, 'updater')->hiddenInput(['value'=>true])->label(false); ?>

<div class="form-group">
    <div class="">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success send-data']) ?>
    </div>
</div>
<?php ActiveForm::end() ?>
<h3 class="mt-5">Шрифт</h3>
<label for="">Текущий шрифт:</label> <b><?=$fname?></b>
<div class="clearfix"></div>
<label for="">Url шрифта:</label> <b><?=$furl?></b>
<div class="clearfix"></div>
<h4>Изменить шрифт на</h4>
<div class="clearfix"></div>
<form action="/backend/web/templates/changefont" method="POST">
    <select name="changeFont" id="" class="form-control" style="max-width:300px;">
        <?php foreach($options as $option){?>
        <option value="<?=$option['id']?>"><?=$option['name']?></option>
        <?php }?>
    </select>
    <input type="hidden" class="hidden" name="id" value="<?=$id?>">
    <input type="hidden" name="_csrf-backend" value="<?=$token?>" />
    <input type="submit" class="btn btn-primary mt-3 mb-5" value="Изменить">
</form>
<a href="/backend/web/templates/" class="btn btn-success">Назад</a>
